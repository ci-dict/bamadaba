local M = {}

M.entries_to_lexicon = function(lexicon, blocks)
  for _, block in ipairs(blocks) do
    local key_value = block.lx and block.lx[1]
    local abbr = block.ps and block.ps[1]
    local semantics = block.sm and block.sm[1]

    if key_value then
      -- Create the new entry with the required structure
      local new_entry = {
        k = {
          {
            ["+@xml:lang"] = "bm",
            ["+content"] = key_value,
          },
        },
        def = {
          {
            ["+@freq"] = "54",
            def = {
              {
                ["+@xml:lang"] = "fr",
                def = {
                  {
                    gr = abbr and { { abbr = abbr } } or nil,
                    def = {},
                  },
                },
              },
            },
          },
        },
      }

      -- local sm_def = {}
      -- if block.sm and block.sm[1] then
      --   print("Processing default sm[1]:", block.sm[1])
      --   local sm_array = {}
      --   table.insert(sm_array, {
      --     ["+@type"] = "co",
      --     ["+content"] = block.sm[1] or "",
      --   })
      --
      --   if #sm_array > 0 then
      --     sm_def.co = { sm_array }
      --   end
      -- end
      -- table.insert(new_entry.def[1].def[1].def, sm_def)

      if block.sn then
        -- This block has numbered word senses
        for i, sn in ipairs(block.sn) do
          -- get the ge or gv value withn the current sense (i) but some entries need to fall back to
          -- the default unnumbered single sens.
          -- ----------------------------------------
          -- local single_def = {}
          -- if block.sm and block.sm[1] then
          --   print("Processing default sm[1]:", block.sm[1])
          --   local sm_array = {}
          --   table.insert(sm_array, {
          --     ["+@type"] = "co",
          --     ["+content"] = block.sm[1] or "",
          --   })
          --
          --   if #sm_array > 0 then
          --     single_def.co = { sm_array }
          --   end
          -- end
          -- table.insert(new_entry.def[1].def[1].def, single_def)
          -- ------------------------------------
          local sn_deftext = block[sn].ge and block[sn].ge[1]
            or block[sn].gv and block[sn].gv[1]
            or block.ge and block.ge[1]
            or ""
          print("Processing sn[" .. i .. "]:", sn)
          print("sn_deftext for sn[" .. i .. "]:", sn_deftext)
          if sn_deftext ~= "" then
            local numbered_def = {
              co = {
                ["+@type"] = "sn",
                ["+content"] = sn,
              },
              deftext = sn_deftext,
            }

            if block[sn].xv then
              local ex_array = {}
              for j = 1, #block[sn].xv do
                print("Processing numbered xv[" .. j .. "]:", block[sn].xv[j])
                print("Processing numbered xe[" .. j .. "]:", block[sn].xe[j])
                if block[sn].xv[j] and block[sn].xe[j] and block[sn].xv[j] ~= "" then
                  table.insert(ex_array, {
                    ex_orig = block[sn].xv[j],
                    ex_tran = block[sn].xe[j],
                  })
                end
              end

              if #ex_array > 0 then
                numbered_def.ex = ex_array
              end
            end

            table.insert(new_entry.def[1].def[1].def[1].def, numbered_def)

            if block[sn].sy then
              local sy_array = {}
              for k = 1, #block[sn].sy do
                if block[sn].sy[k] ~= "" then
                  table.insert(sy_array, {
                    ["+@type"] = "syn",
                    ["+content"] = block[sn].sy[k] or "",
                  })
                end
              end

              if #sy_array > 0 then
                numbered_def.sr = { kref = sy_array }
              end
            end
          end
        end
      else
        -- This block does not have numbered word senses
        local deftext = block.ge and block.ge[1] or block.gv and block.gv[1] or ""
        print("Default sn_deftext:", deftext)
        if deftext ~= "" then
          local single_def = {
            deftext = deftext,
          }

          if block.xv then
            local ex_array = {}
            for j = 1, #block.xv do
              print("Processing default xv[" .. j .. "]:", block.xv[j])
              print("Processing default xe[" .. j .. "]:", block.xe[j])
              if block.xv[j] and block.xe[j] and block.xv[j] ~= "" then
                table.insert(ex_array, {
                  ex_orig = block.xv[j],
                  ex_tran = block.xe[j],
                })
              end
            end

            if #ex_array > 0 then
              single_def.ex = ex_array
            end
          end
          table.insert(new_entry.def[1].def[1].def[1].def, single_def)

          if block.sy then
            local sy_array = {}
            for k = 1, #block.sy do
              if block.sy[k] ~= "" then
                table.insert(sy_array, {
                  ["+@type"] = "syn",
                  ["+content"] = block.sy[k] or "",
                })
              end
            end

            if #sy_array > 0 then
              single_def.sr = { kref = sy_array }
            end
          end
          table.insert(new_entry.def[1].def[1].def[1].def, single_def)
        end
      end

      -- Insert the new entry into the ar table
      table.insert(lexicon.lexicon.ar, new_entry)
    end
  end
end

return M
