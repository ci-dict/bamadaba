local json = require "dkjson"

-- Function to split a string by a delimiter
local function split(str, delim)
  local result = {}
  for match in (str .. delim):gmatch("(.-)" .. delim) do
    table.insert(result, match)
  end
  return result
end

-- Function to trim whitespace from both ends of a string
local function trim(s)
  return s:match "^%s*(.-)%s*$"
end

local function parse_block(block)
  local block_table = {}
  for line in block:gmatch "[^\r\n]+" do
    local key, value = line:match "\\(%a+) (.+)"
    if key and value then
      key = trim(key)
      value = trim(value)
      if key == "ge" then
        value = value:gsub("%.", " ")
      end
      if not block_table[key] then
        block_table[key] = {}
      end
      table.insert(block_table[key], value)
    end
  end
  return block_table
end

-- Read the file content
local function read_file(filename)
  local file = io.open(filename, "r")
  if not file then
    error("Could not open file " .. filename)
  end
  local content = file:read "*all"
  file:close()
  return content
end

-- Function to load the lexicon table from bamadaba.json
local function load_json(filename)
  local file = io.open(filename, "r")
  if not file then
    error("Could not open file " .. filename)
  end
  local content = file:read "*all"
  file:close()
  return json.decode(content)
end

-- Function to save the lexicon table to bamadaba.json
local function save_json(filename, table)
  local file = io.open(filename, "w")
  if not file then
    error("Could not open file " .. filename .. " for writing")
  end
  local json_content = json.encode(table, { indent = true })
  file:write(json_content)
  file:close()
end

-- Function to add new entries to the lexicon table
local function add_new_entries_to_lexicon(lexicon, blocks)
  for _, block in ipairs(blocks) do
    local key_value = block.lx and block.lx[1]
    local abbr = block.ps and block.ps[1]

    if key_value then
      -- Create the new entry with the required structure
      local new_entry = {
        k = {
          {
            ["+content"] = key_value,
            ["+@xml:lang"] = "bm",
          },
        },
        def = {
          {
            ["+@freq"] = "54",
            def = {
              {
                ["+@xml:lang"] = "fr",
                def = {},
              },
            },
            sr = block.sr and {
              kref = {
                {
                  ["+content"] = block.sr[1],
                  ["+@type"] = "spv",
                },
              },
            } or nil,
          },
        },
      }

      if block.sn then
        for i, sn in ipairs(block.sn) do
          local sn_deftext = (block.ge or block.gv) and (block.ge and block.ge[i] or block.gv and block.gv[i]) or ""
          if sn_deftext ~= "" then
            local sn_def = {
              co = {
                {
                  ["+content"] = sn, -- Correctly set the sn value
                  ["+@type"] = "sn",
                },
              },
              deftext = sn_deftext,
              sr = {
                kref = {
                  ["+content"] = block.sr and block.sr[i] or "",
                  ["+@type"] = "syn",
                },
              },
              categ = block.categ and block.categ[i] or "",
            }

            table.insert(new_entry.def[1].def[1].def, sn_def)
          end
        end
      else
        local deftext = block.ge and block.ge[1] or block.gv and block.gv[1] or ""
        if deftext ~= "" then
          local sn_def = {
            deftext = deftext,
            sr = {
              kref = {
                ["+content"] = block.sr and block.sr[1] or "",
                ["+@type"] = "syn",
              },
            },
            categ = block.categ and block.categ[1] or "",
          }

          table.insert(new_entry.def[1].def[1].def, sn_def)
        end
      end

      -- Insert the new entry into the ar table
      table.insert(lexicon.lexicon.ar, new_entry)
    end
  end
end

local function main()
  local input_filename = "bamadaba.txt"
  local lexicon_filename = "bamadaba.json"

  -- Read and parse input.txt
  local content = read_file(input_filename)
  local blocks = split(content, "\n\n")
  local parsed_blocks = {}
  for _, block in ipairs(blocks) do
    table.insert(parsed_blocks, parse_block(block))
  end

  -- Initialize a new lexicon table with an empty ar array
  local lexicon = { lexicon = { ar = {} } }

  -- Add new entries to the lexicon table
  add_new_entries_to_lexicon(lexicon, parsed_blocks)

  -- Save the modified lexicon table to bamadaba.json
  save_json(lexicon_filename, lexicon)
end

-- Call the main function to execute the script
main()
