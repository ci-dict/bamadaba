-- Function to add new entries to the lexicon table
local M = {}

M.entries_to_lexicon = function(lexicon, blocks)
  for _, block in ipairs(blocks) do
    local key_value = block.lx and block.lx[1]
    local abbr = block.ps and block.ps[1]

    -- print("Processing block:", block)
    if key_value then
      -- Create the new entry with the required structure
      local new_entry = {
        k = {
          {
            ["+@xml:lang"] = "bm",
            ["+content"] = key_value,
          },
        },
        def = {
          {
            ["+@freq"] = "54",
            def = {
              {
                ["+@xml:lang"] = "fr",
                def = {
                  {
                    gr = abbr and { { abbr = abbr } } or nil,
                    def = {},
                  },
                },
              },
            },
            sr = block.sr and {
              kref = {
                {
                  ["+@type"] = "spv",
                  ["+content"] = block.sr[1],
                },
              },
            } or nil,
          },
        },
      }

      if block.sn then
        -- this block has numbered word senses
        for i, sn in ipairs(block.sn) do
          local sn_deftext = block[sn].ge and block[sn].ge[1] or block[sn].gv and block[sn].gv[1] or ""
          print("Processing sn[" .. i .. "]:", sn)
          print("sn_deftext for sn[" .. i .. "]:", sn_deftext)
          if sn_deftext ~= "" then
            local numbered_def = {
              co = {
                ["+@type"] = "sn",
                ["+content"] = sn,
              },
              deftext = sn_deftext,
              sr = {
                kref = {
                  ["+@type"] = "syn",
                  ["+content"] = block[sn].sy and block[sn].sy[1] or "",
                },
              },
              -- categ = block[sn].categ and block[sn].categ[1] or "",
              -- ex = {},
            }

            if block[sn].xv and block[sn].xe then
              local ex_array = {}
              for j = 1, #block[sn].xv do
                -- print("Processing unnumbered xv[" .. j .. "]:", block[sn].xv[j])
                -- print("Processing unnumbered xe[" .. j .. "]:", block[sn].xe[j])
                if block[sn].xv[j] and block[sn].xe[j] and block[sn].xv[j] ~= "" then
                  table.insert(ex_array, {
                    ex_orig = block[sn].xv[j],
                    ex_tran = block[sn].xe[j],
                  })
                end
              end

              if #ex_array > 0 then
                numbered_def.ex = ex_array
              end
            end

            table.insert(new_entry.def[1].def[1].def[1].def, numbered_def)
          end
        end
      else
        -- this block does not have numbered word senses
        local deftext = block.ge and block.ge[1] or block.gv and block.gv[1] or ""
        -- print("Default sn_deftext:", deftext)
        if deftext ~= "" then
          local single_def = {
            deftext = deftext,
            sr = {
              kref = {
                ["+content"] = block.sr and block.sr[1] or "",
                ["+@type"] = "syn",
              },
            },
            categ = block.categ and block.categ[1] or "",
          }

          if block.xv and block.xe then
            local ex_array = {}
            for j = 1, #block.xv do
              -- print("Processing default xv[" .. j .. "]:", block.xv[j])
              -- print("Processing default xe[" .. j .. "]:", block.xe[j])
              if block.xv[j] and block.xe[j] and block.xv[j] ~= "" then
                table.insert(ex_array, {
                  ex_orig = block.xv[j],
                  ex_tran = block.xe[j],
                })
              end
            end

            if #ex_array > 0 then
              single_def.ex = ex_array
            end
          end

          table.insert(new_entry.def[1].def[1].def[1].def, single_def)
        end
      end

      -- Insert the new entry into the ar table
      table.insert(lexicon.lexicon.ar, new_entry)
    else
      -- print "Skipping block due to missing key_value"
    end
  end
end

return M
