#!/bin/awk -f

BEGIN {
    # Initialize variables
    entry = ""
    in_entry = 0
}

# Function to print an entry in TSV format
function print_entry(entry) {
    if (entry != "") {
        # Replace newlines within fields with spaces
        gsub("\n", " ", entry)
        print entry
    }
}

# Main processing
{
    # If the line starts with a backslash, it's the start of a new entry
    if ($1 ~ /^\\/) {
        if (in_entry) {
            # If we were already in an entry, print it in TSV format
            print_entry(entry)
        }
        entry = ""
        in_entry = 1
    }

    # Append the line to the current entry
    entry = entry $0 "\n"
}

# End of file, print the last entry
END {
    print_entry(entry)
}

