local json = require "dkjson"
local add_entries = require "module7"

-- Function to split a string by a delimiter
local function split(str, delim)
  local result = {}
  for match in (str .. delim):gmatch("(.-)" .. delim) do
    table.insert(result, match)
  end
  return result
end

-- Function to trim whitespace from both ends of a string
local function trim(s)
  return s:match "^%s*(.-)%s*$"
end

-- Parse block with `sn` and `ex` processing
local function parse_block(block)
  local block_table = {}
  local current_sn = nil

  for line in block:gmatch "[^\r\n]+" do
    local key, value = line:match "\\(%a+) (.+)"
    if key and value then
      key = trim(key)
      value = trim(value)

      if key == "ge" then
        value = value:gsub("%.", " ")
      end

      if key == "sn" then
        current_sn = value
        if not block_table[key] then
          block_table[key] = {}
        end
        table.insert(block_table[key], current_sn)
      else
        if current_sn then
          if not block_table[current_sn] then
            block_table[current_sn] = {}
          end
          if not block_table[current_sn][key] then
            block_table[current_sn][key] = {}
          end
          table.insert(block_table[current_sn][key], value)
        else
          if not block_table[key] then
            block_table[key] = {}
          end
          table.insert(block_table[key], value)
        end
      end
    end
  end

  return block_table
end

-- Read the file content
local function read_file(filename)
  local file = io.open(filename, "r")
  if not file then
    error("Could not open file " .. filename)
  end
  local content = file:read "*all"
  file:close()
  return content
end

-- Function to load the lexicon table from bamadaba.json
local function load_json(filename)
  local file = io.open(filename, "r")
  if not file then
    error("Could not open file " .. filename)
  end
  local content = file:read "*all"
  file:close()
  return json.decode(content)
end

-- Function to save the lexicon table to bamadaba.json
local function save_json(filename, table)
  local file = io.open(filename, "w")
  if not file then
    error("Could not open file " .. filename .. " for writing")
  end
  local json_content = json.encode(table, { indent = true })
  file:write(json_content)
  file:close()
end

local function main()
  local input_filename = "bamadaba.txt"
  local lexicon_filename = "bamadaba.json"

  -- Read and parse input.txt
  local content = read_file(input_filename)
  local blocks = split(content, "\n\n")
  local parsed_blocks = {}
  for _, block in ipairs(blocks) do
    local parsed_block = parse_block(block)
    -- print("Parsed block:", parsed_block)
    table.insert(parsed_blocks, parsed_block)
  end

  -- Initialize a new lexicon table with an empty ar array
  local lexicon = { lexicon = { ar = {} } }

  -- Add new entries to the lexicon table
  add_entries.entries_to_lexicon(lexicon, parsed_blocks)

  -- Save the modified lexicon table to bamadaba.json
  save_json(lexicon_filename, lexicon)
end

main()
