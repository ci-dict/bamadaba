#!/usr/bin/bash
#set -o errexit
# set -o nounset
# set -o pipefail

for x in jq yq csvclean; do
  type -P $x >/dev/null 2>&1 || {
    echo >&2 "${x} not installed.  Aborting."
    exit 0
  }
done

#set -o errexit
file=bamadaba
for x in tsv tmp xml xsd xsl adoc json yml; do export "${x}=${file}.$x"; done
project=bamadaba
dict="./$project/dict.xdxf"
branch=dev
# webtarget="$HOME"/dev/svelte/coastsystems.net/src
# dyu_lex="$HOME"/dev/svelte/dyu-lex
source ./functions.sh

#checkbranch "$webtarget" "$branch"
#checkbranch "$dyu_lex" "$branch"

#[[ -f "$project/$xml" ]] && count_xml "$project/$xml"

# DATE=$(date -I) && export DATE
# yq -i '.xdxf.meta_info.last_edited_date = strenv(DATE)' "$yml"
# yamlfmt "$yml"
[[ -f "$json" ]] && rm "$json"
lua ./import.lua

yq -pj -oy "$json" >"~bamadaba.yml"

yq -py -oy '.xdxf.lexicon = load("~bamadaba.yml").lexicon' meta_info.yml |
  yq -py -oy '.xdxf.lexicon.ar[] |= pick(["k", "def"])' |
  yq -py -oy '.xdxf.lexicon.ar[].def[].def[].def[] |= pick(["gr", "def"])' |
  yq -py -oy '.xdxf.lexicon.ar[].def[].def[].def[].def[] |= pick(["co", "deftext", "ex", "sr", "categ"])' |
  yq -py -oy '.xdxf.lexicon.ar[].def[].def[].def[].def[].ex[] |= pick(["ex_orig", "ex_tran"])' >"$yml"
#  >"$yml"

DATE=$(date -I) && export DATE
yq -i '.xdxf.meta_info.last_edited_date = strenv(DATE)' "$yml"
yamlfmt "$yml"

yq -ox "$yml" >"$dict"
#xmllint --noout --dtdvalid xdxf_strict.dtd ./$dict
ln -v -f $dict "./$project/$xml"
[[ -f "$project/$xml" ]] && count_xml "$project/$xml"

echo "~$yml"
rm -v "~$yml"

exit
#yq -py -oy '.xdxf.lexicon.ar[].def[].def[].def[0].def[] |= pick(["deftext", "ex", "sr", "categ"])' |
#make new xml xdxf from yml
# yq -oj "$yml" >"$project/$json"
# yq -ox "$yml" >"$dict"
# xmllint --noout --dtdvalid xdxf_strict.dtd ./$dict
# ln -v -f $dict "./$project/$xml"

#now we have a new xml file.  sort it back to the yaml
#xslt3 -xsl:$project-sort.xsl -s:"$project/$xml" >"~sorted-$xml"
#make sure that all k elements are converted back to arrays
#yq does not have if statement...
# xslt3 -xsl:$project-sort.xsl -s:"$project/$xml" |
#   yq -px -oy '
#   .xdxf.lexicon.ar[].def |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].def |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].def[].def |= ([] + .) |
#   .xdxf.lexicon.ar[].k |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].gr |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].def[0].def[] |= pick(["deftext", "ex", "sr", "categ"]) |
#   .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0].+@type // "foo" |= . |
#   .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0].ex_orig // "bar" |= . |
#  .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0] |= select(.["+@type"] == null) |= .ex_orig = "" |
#  .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0] |= select(.ex_orig == "") |= .+@type = "exm"
# ' >"new-$yml"
#' "~sorted-$xml" >"new-$yml"
#add these later as they blow up second time round
#yq adds a null valut to kref wich is ok but the next script run the // will change the nul to []
# and this does not pass the dtd
# investigate testing for content and if none then delete???
# .xdxf.lexicon.ar[].def[].def[].def[0].def[].sr.kref |= ([] + .) |
# .xdxf.lexicon.ar[].def[].def[].def[0].def[].categ |= ([] + .) |

# count_yml "$yml"
# count_yml "$yml"
# mv -v "new-$yml" "$yml"
# cp -v "$yml" "./archive/$(date -Im)-$yml"
# #sed -i "2i +directive: \"DOCTYPE xdxf SYSTEM 'xdxf_strict.dtd'\"" "$yml"
# sed -i "2i +directive: DOCTYPE xdxf SYSTEM 'xdxf_strict.dtd'" "$yml"
# count_yml "$yml"
# count_xml "$project/$xml"
# yamlfmt "$yml"
#rm ./~*

#yq -py -oj "$yml" >"$project/$json"
