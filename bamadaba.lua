local lexicon = {
  ar = {
    {
      k = {
        {
          ["+content"] = "",
          ["+@xml:lang"] = "bm",
        },
      },
      def = {
        {
          def = {
            {
              ["+@xml:lang"] = "fr",
              def = {
                {
                  tr = "",
                  gr = {
                    abbr = "",
                  },
                  co = {
                    ["+@type"] = "uuid",
                    ["+content"] = "",
                  },
                  def = {
                    {
                      co = {
                        ["+@type"] = "uuid",
                        ["+content"] = "",
                      },
                      deftext = "",
                      ex = {
                        {
                          ["+@type"] = "exm",
                          ex_orig = "",
                          ex_tran = "",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
          sr = {
            kref = {
              ["+content"] = "",
              ["+@type"] = "",
            },
          },
        },
      },
    },
  },
}

return lexicon
