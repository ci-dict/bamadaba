#!/bin/awk -f

BEGIN {
    print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    print "<dictionary>"
}

# Function to escape special XML characters
function escape_xml(str) {
    gsub("&", "&amp;", str)
    gsub("<", "&lt;", str)
    gsub(">", "&gt;", str)
    return str
}

/^\\lx/ {
    if (in_entry) {
        print "</entry>"
    }
    in_entry = 1
    print "<entry>"
    print "<lx>" escape_xml($2) "</lx>"
}

/^\\sn/ {
    print "<sn>" escape_xml($2) "</sn>"
}

/^\\ps/ {
    print "<ps>" escape_xml($2) "</ps>"
}

/^\\ge/ {
    print "<ge>" escape_xml($2) "</ge>"
}

/^\\ee/ {
    print "<ee>" escape_xml($2) 
    in_definition = 1
}

/^\\xv/ {
    if (in_definition) {
        print "<xv>" escape_xml($2) "</xv>"
    }
}

/^\\xe/ {
    if (in_definition) {
        print "<xe>" escape_xml($2) "</xe>"
    }
}

END {
    if (in_entry) {
        print "</entry>"
    }
    print "</dictionary>"
}

