-- Load the lexicon table from bamadaba.lua
local lexicon = require "bamadaba"
local json = require "dkjson"

-- Save the lexicon table to bamadaba.json
local function save_to_json(filename, table)
  local file = io.open(filename, "w")
  if not file then
    error("Could not open file " .. filename .. " for writing")
  end
  local json_content = json.encode(table, { indent = true })
  file:write(json_content)
  file:close()
end

save_to_json("bamadaba.json", lexicon)
